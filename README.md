# concurrency-demo

Concurrency demo  using java8 
* 1.Creating thread 
* 2.RUnnable/callable
* 3.Synchrnozers
* 4.Lock/ReentrantLock, StampedLock
* 5.Wait/notify/notifyall/sleep
* 6.Yield/join
* 7.Volatile
* 8.Threadlocal
* 9.BusySpinning
* 10.Concurrent Collections
* 11.Blocking Queueus / Map
* 12.Atomic Variables
* 13.Fork-join
* 14.Future
* 15.Completable Future
* 16.FileReader/FileWriteer
