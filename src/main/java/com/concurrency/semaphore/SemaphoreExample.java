package com.concurrency.semaphore;



import java.util.concurrent.Semaphore;

public class SemaphoreExample {
    static Semaphore semaphore = new Semaphore(4);

    static class FileWriterThread extends Thread {
        String name = "";
        FileWriterThread(String name) {
            this.name = name;
        }

        public void run() {
            try {
                System.out.println(name + " : acquiring lock... by thread"+Thread.currentThread().getName());
                System.out.println(name + " : available Semaphore permits now: "
                        + semaphore.availablePermits());
                semaphore.acquire();
                System.out.println(name + " : got the permit!");
                try {
                    for (int i = 1; i <= 5; i++) {
                        System.out.println(name + " : is performing operation " + i
                                + ", available Semaphore permits : "
                                + semaphore.availablePermits());
                        // sleep 1 second
                        Thread.sleep(1000);
                    }
                } finally {
                    // calling release() after a successful acquire()
                    System.out.println(name + " : releasing lock...");
                    semaphore.release();
                    System.out.println(name + " : available Semaphore permits now: "
                            + semaphore.availablePermits());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
        System.out.println("Total available Semaphore permits : "
                + semaphore.availablePermits());
        FileWriterThread t1 = new FileWriterThread("A");
        t1.start();
        FileWriterThread t2 = new FileWriterThread("B");
        t2.start();
        FileWriterThread t3 = new FileWriterThread("C");
        t3.start();
        FileWriterThread t4 = new FileWriterThread("D");
        t4.start();
        FileWriterThread t5 = new FileWriterThread("E");
        t5.start();
        FileWriterThread t6 = new FileWriterThread("F");
        t6.start();
    }
}