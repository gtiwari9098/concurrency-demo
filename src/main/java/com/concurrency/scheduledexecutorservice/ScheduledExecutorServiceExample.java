package com.concurrency.scheduledexecutorservice;

import java.util.concurrent.*;

public class ScheduledExecutorServiceExample {
    public static void main(String[] args) throws InterruptedException, ExecutionException {

        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        Callable<Integer> task2 = () -> 10;
        //run this task after 5 seconds, nonblock for task3, returns a future
        ScheduledFuture<Integer> scheduleWithFixedDelay = ses.schedule(task2, 5, TimeUnit.SECONDS);
        ScheduledFuture<?> scheduleWithFiexedrate = ses.scheduleAtFixedRate( new ExecutorRunnableTask("Callable task"), 5,5, TimeUnit.SECONDS);

        // block and get the result
        System.out.println("fetching scheduled callable task => "+scheduleWithFixedDelay.get());
        System.out.println("Fetching scheduled runnable task =>"+scheduleWithFiexedrate.get());
        System.out.println("shutdown!");
        ses.shutdown();
    }


}

class ExecutorRunnableTask implements Runnable {
    private String name;

    public ExecutorRunnableTask(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {
        try {
            Long duration = (long) (Math.random() * 10);
            System.out.println("Doing a runnable task during : " + name + " - " + Thread.currentThread().getName());
            TimeUnit.SECONDS.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


    class ExecutorCallableTask implements Callable {
        private String name;

        public ExecutorCallableTask(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public Long call() {
            try {
                Long duration = (long) (Math.random() * 10);
                System.out.println("Doing a callable task during : " + name + " - " + Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(duration);
                return duration;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

