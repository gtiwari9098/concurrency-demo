package com.concurrency.completablefuture;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;


public class CompletablefutureExample {
    public static void main(String[] args) {

    	runAsyncExample();
    }

    //creating a completed compltetable Future
    static void completedFutureExample() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message");
    }

    //Running a Simple Asynchronous Stage
    static void runAsyncExample() {
        CompletableFuture<Void> cf = CompletableFuture.runAsync(() -> {
            Math.random();
            System.out.println("doing...");
        });
        System.out.println(cf.isDone());
        sleepEnough();
        System.out.println(cf.isDone());
    }

    private static void sleepEnough() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Applying a function on previous stage
    static void thenApplyExample() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message").thenApply(s -> {
            return s.toUpperCase();
        });
        System.out.println("MESSAGE"+ cf.getNow(null));
    }

    //Asynchronously Applying a Function on a Previous Stage
    //By appending the Async suffix to the method in the previous example,
    // the chained CompletableFuture would execute asynchronously (using ForkJoinPool.commonPool()).
    static void thenApplyAsyncExample() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message").thenApplyAsync(s -> {
            return s.toUpperCase();
        });
        System.out.println(cf.getNow(null));
        System.out.println("MESSAGE"+ cf.join());
    }

    //Asynchronously Applying a Function on  a Previous Stage Using a Custom Executor
    static ExecutorService executor = Executors.newFixedThreadPool(3, new ThreadFactory() {
        int count = 1;

        @Override
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "custom-executor-" + count++);
        }
    });

    static void thenApplyAsyncWithExecutorExample() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message").thenApplyAsync(s -> {
            return s.toUpperCase();
        }, executor);
        System.out.println(cf.getNow(null));
        System.out.println("MESSAGE"+ cf.join());
    }


    //Consuming the Result of the Previous Stage
    static void thenAcceptExample() {
        StringBuilder result = new StringBuilder();
        CompletableFuture.completedFuture("thenAccept message")
                .thenAccept(s -> result.append(s));
        System.out.println("Result was empty"+( result.length() > 0));

    }

    //Asynchronously Consuming the Result of the Previous Stage
    static void thenAcceptAsyncExample() {
        StringBuilder result = new StringBuilder();
        CompletableFuture<Void> cf = CompletableFuture.completedFuture("thenAcceptAsync message")
                .thenAcceptAsync(s -> result.append(s));
        cf.join();
        System.out.println("Result was empty"+( result.length() > 0));
    }

    //Completing a Computation Exceptionally and delaying asynchronous task
    static void completeExceptionallyExample() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message").thenApplyAsync(String::toUpperCase,
                CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS));
        CompletableFuture<String> exceptionHandler = cf.handle((s, th) -> {
            return (th != null) ? "message upon cancel" : "";
        });
        cf.completeExceptionally(new RuntimeException("completed exceptionally"));
        try {
            cf.join();
        } catch (CompletionException ex) { // just for testing
            System.out.println("completed exceptionally"+ ex.getCause().getMessage());
        }
        System.out.println("message upon cancel"+ exceptionHandler.join());
    }

    // Cancelling a computation
    static void cancelExample() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message").thenApplyAsync(String::toUpperCase,
                CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS));
        CompletableFuture<String> cf2 = cf.exceptionally(throwable -> "canceled message");
        System.out.println("Was not canceled" + cf.cancel(true));
        System.out.println("Was not completed exceptionally" + cf.isCompletedExceptionally());
        System.out.println("canceled message" + cf2.join());
    }

    //Applying a Function to the Result of Either of Two Completed Stages
    static void applyToEitherExample() {
        String original = "Message";
        CompletableFuture<String> cf1 = CompletableFuture.completedFuture(original)
                .thenApplyAsync(s -> delayedUpperCase(s));
        CompletableFuture<String> cf2 = cf1.applyToEither(
                CompletableFuture.completedFuture(original).thenApplyAsync(s -> delayedLowerCase(s)),
                s -> s + " from applyToEither");
        System.out.println(cf2.join().endsWith(" from applyToEither"));
    }

    //Consuming the Result of Either of Two Completed Stages
    static void acceptEitherExample() {
        String original = "Message";
        StringBuffer result = new StringBuffer();
        CompletableFuture<Void> cf = CompletableFuture.completedFuture(original)
                .thenApplyAsync(s -> delayedUpperCase(s))
                .acceptEither(CompletableFuture.completedFuture(original).thenApplyAsync(s -> delayedLowerCase(s)),
                        s -> result.append(s).append("acceptEither"));
        cf.join();
        System.out.println("Result was empty" + result.toString().endsWith("acceptEither"));
    }

    //Running a Runnable Upon Completion of Both Stages
    static void runAfterBothExample() {
        String original = "Message";
        StringBuilder result = new StringBuilder();
        CompletableFuture.completedFuture(original).thenApply(String::toUpperCase).runAfterBoth(
                CompletableFuture.completedFuture(original).thenApply(String::toLowerCase),
                () -> result.append("done"));
        System.out.println("Result was empty" + (result.length() > 0));
    }

    //Accepting the Results of Both Stages in a BiConsumer

    static void thenAcceptBothExample() {
        String original = "Message";
        StringBuilder result = new StringBuilder();
        CompletableFuture.completedFuture(original).thenApply(String::toUpperCase).thenAcceptBoth(
                CompletableFuture.completedFuture(original).thenApply(String::toLowerCase),
                (s1, s2) -> result.append(s1 + s2));
        System.out.println("MESSAGEmessage" + result.toString());
    }

    //Asynchronously Applying a BiFunction on Results of Both Stages

    static void thenCombineAsyncExample() {
        String original = "Message";
        CompletableFuture<String> cf = CompletableFuture.completedFuture(original)
                .thenApplyAsync(s -> delayedUpperCase(s))
                .thenCombine(CompletableFuture.completedFuture(original).thenApplyAsync(s -> delayedLowerCase(s)),
                        (s1, s2) -> s1 + s2);
        System.out.println("MESSAGEmessage" + cf.join());
    }

    private static String delayedLowerCase(String s) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return s.toLowerCase();
    }

    //Composing CompletableFutures
    static void thenComposeExample() {
        String original = "Message";
        CompletableFuture<String> cf = CompletableFuture.completedFuture(original).thenApply(s -> delayedUpperCase(s))
                .thenCompose(upper -> CompletableFuture.completedFuture(original).thenApply(s -> delayedLowerCase(s))
                        .thenApply(s -> upper + s));
        System.out.println("MESSAGEmessage"+( cf.join()));
    }

    //Creating a Stage That Completes When Any of Several Stages Completes
    static void anyOfExample() {
        StringBuilder result = new StringBuilder();
        List<String> messages = Arrays.asList("a", "b", "c");
        List<CompletableFuture<String>> futures = messages.stream()
                .map(msg -> CompletableFuture.completedFuture(msg).thenApply(s -> delayedUpperCase(s)))
                .collect(Collectors.toList());
        CompletableFuture.anyOf(futures.toArray(new CompletableFuture[futures.size()])).whenComplete((res, th) -> {
            if (th == null) {
                System.out.println(isUpperCase((String) res));
                result.append(res);
            }
        });
        System.out.println("Result was empty"+( result.length() > 0));
    }

    //Creating a Stage That Completes When All Stages Complete
    static void allOfExample() {
        StringBuilder result = new StringBuilder();
        List<String> messages = Arrays.asList("a", "b", "c");
        List<CompletableFuture<String>> futures = messages.stream()
                .map(msg -> CompletableFuture.completedFuture(msg).thenApply(s -> delayedUpperCase(s)))
                .collect(Collectors.toList());
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).whenComplete((v, th) -> {
            futures.forEach(cf -> System.out.println(isUpperCase(cf.getNow(null))));
            result.append("done");
        });
        System.out.println("Result was empty"+( result.length() > 0));
    }

    //Creating a Stage That Completes Asynchronously When All Stages Complete
    static void allOfAsyncExample() {
        StringBuilder result = new StringBuilder();
        List<String> messages = Arrays.asList("a", "b", "c");
        List<CompletableFuture<String>> futures = messages.stream()
                .map(msg -> CompletableFuture.completedFuture(msg).thenApplyAsync(s -> delayedUpperCase(s)))
                .collect(Collectors.toList());
        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]))
                .whenComplete((v, th) -> {
                    futures.forEach(cf -> System.out.println(isUpperCase(cf.getNow(null))));
                    result.append("done");
                });
        allOf.join();
        System.out.println("Result was empty"+( result.length() > 0));
    }

    private static boolean isUpperCase(String now) {
        return now.toUpperCase() == now;
    }

    private static String delayedUpperCase(String s) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return s.toUpperCase();
    }

}

