package com.concurrency.stampedlock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.StampedLock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


//new kind of lock StampedLock is added which apart from providing separate read and write locks also has a feature
// for optimistic locking for read operations.
//StampedLock in Java also provides method to upgrade read lock to write lock which is not there in ReentrantReadWriteLock in Java.
//The locking methods of StampedLock in Java return a stamp represented by a long value
//You can use these stamps to either release a lock, to check if the lock is still valid, to convert a lock.
//Read Lock
//WriteLock
//Optimistic Reading That is the new mode added in StampedLock. Method
//tryOptimisticRead() is used to read in optimistic mode. This method returns a non-zero stamp only if the lock is not currently held in write mode.
//Method validate(long) is used to validate if the values read optimistically are correct or not.
// Validate() method returns true if the lock has not been acquired in write mode since obtaining a given stamp.

//method tryConvertToWriteLock(long)attempts to "upgrade" a mode, returning a valid write stamp if
//
  //      (1) already in writing mode
    //    (2) in reading mode and there are no other readers or
      //  (3) in optimistic mode and the lock is available.

 //Unlike ReentrantLocks, StampedLocks are not reentrant, so locked bodies should not call other unknown methods that may try to re-acquire locks (although you may pass a stamp to other methods that can use or convert it).
public class StampedLockExample {
     public static final Log LOG = LogFactory.getLog(StampedLockExample.class);
     private static final int NUMBER_OF_TAX_PAYER = 1000;
     private static IncomeTaxDept incomeTaxDept = new IncomeTaxDept(1000, NUMBER_OF_TAX_PAYER);
     public static void main(String[] args) {
         registerTaxPayers();
         ExecutorService executor = Executors.newFixedThreadPool(30);
         LOG.info("Initial IncomeTax Department's total revenue is ===>> "+incomeTaxDept.getTotalRevenue());
         for(TaxPayer taxPayer : incomeTaxDept.getTaxPayersList())
             executor.submit(() -> {
                         try {
                             Thread.sleep(100);
                             incomeTaxDept.payTax(taxPayer);
                             double revenue = incomeTaxDept.getTotalRevenue();
                             LOG.info("IncomeTax Department's total revenue after tax paid at client code is --->> " +revenue);
                             double returnAmount = incomeTaxDept.getFederalTaxReturn(taxPayer);
                             LOG.info(taxPayer.getTaxPayerName() + " received the Federal return amount = " +returnAmount);
                             revenue = incomeTaxDept.getTotalRevenueOptimisticRead();
                             LOG.info("IncomeTax Department's total revenue after getting Federal tax return at client code is --->> " +revenue);
                             double stateReturnAmount = incomeTaxDept.getStateTaxReturnUisngConvertToWriteLock(taxPayer);
                             LOG.info(taxPayer.getTaxPayerName() + " received the State tax return amount = " +stateReturnAmount);
                             revenue = incomeTaxDept.getTotalRevenueOptimisticRead();
                             LOG.info("IncomeTax Department's total revenue after getting State tax return at client code is --->> " +revenue);
                             Thread.sleep(100);
                         } catch (Exception e) {}
                     }
             );
         executor.shutdown();
     }
     private static void registerTaxPayers(){
         for(int i = 1; i < NUMBER_OF_TAX_PAYER+1; i++){
             TaxPayer taxPayer = new TaxPayer();
             taxPayer.setTaxAmount(2000);
             taxPayer.setTaxPayerName("Payer-"+i);
             taxPayer.setTaxPayerSsn("xx-xxx-xxxx"+i);
             incomeTaxDept.registerTaxPayer(taxPayer);
         }
     }
 }