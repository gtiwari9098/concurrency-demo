package com.concurrency.delayqueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

public class DelayQueueExample {

    /**
     * @param args
     */
    public static void main(String[] args) {

        // Creates an instance of blocking queue using the DelayQueue.
        BlockingQueue<DelayedObject> queue = new DelayQueue();

        // Starting DelayQueue Producer to push some delayed objects to the queue
        Thread producerThread = new Thread(new DelayQueueProducer(queue), "producer thread");
        producerThread.start();

        // Starting DelayQueue Consumer to take the expired delayed objects from the queue
        Thread consumerThread = new Thread(new DelayQueueConsumer(queue), "consumer thread ");
        consumerThread.start();

    }

}