package com.concurrency.delayqueue;

import com.concurrency.creation.CompleteRunnableExample;

import java.util.concurrent.BlockingQueue;

public class DelayQueueConsumer implements Runnable {

    private BlockingQueue<DelayedObject> queue;

    public DelayQueueConsumer(BlockingQueue queue) {
        super();
        this.queue = queue;
    }
    @Override
    public void run() {
        while (true) {
            try {
                // Take elements out from the DelayQueue object.
                DelayedObject object = queue.take();
                System.out.printf("[%s] - Take object = %s%n",
                        Thread.currentThread().getName(), object);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}