package com.concurrency.forkjoin;

import java.util.concurrent.ForkJoinPool;

public class MyForkJoinPoolMain {
    public static void main(String[] args) {

        //creates a ForkJoinPool with a parallelism level of 4.
        ForkJoinPool forkJoinPool = new ForkJoinPool(4);

        MyRecursiveAction myRecursiveAction = new MyRecursiveAction(24);
        forkJoinPool.invoke(myRecursiveAction);


        MyRecursiveTask myRecursiveTask = new MyRecursiveTask(128);
        long mergedResult = forkJoinPool.invoke(myRecursiveTask);
        System.out.println("mergedResult = " + mergedResult);
    }
}
