package com.concurrency.prodconsumer;

public class Consumer implements Runnable {
    private TestQueue testQueue;
    private boolean stopping =false;
    public Consumer(TestQueue testQueue) {
        this.testQueue = testQueue;
    }

    public boolean  doStop(){
       return stopping == false;
    }

    public void stopNow (){
        stopping = true;
    }
    @Override
    public void run() {
        while(doStop()) {
            try {
              String msg =  testQueue.getMessage();
              System.out.println(" Msg consumed =>" + msg);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
