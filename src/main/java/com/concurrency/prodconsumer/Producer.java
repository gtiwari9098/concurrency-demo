package com.concurrency.prodconsumer;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Producer implements Runnable{
    private boolean stopping =false;
    private TestQueue testQueue;
    public Producer(TestQueue testQueue) {
        this.testQueue = testQueue;
    }

    @Override
    public void run() {
        while (doStop()) {
            try {
                String msg = LocalDateTime.now().toString();
                testQueue.putMessage(LocalDateTime.now().toString());
                System.out.println(" Msg produced =>" + msg);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean  doStop(){
        return stopping == false;
    }

    public void stopNow (){
        stopping = true;
    }
}
