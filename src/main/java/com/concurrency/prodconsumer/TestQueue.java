package com.concurrency.prodconsumer;

import java.util.ArrayList;

public class TestQueue {
    ArrayList<String> blocklist;
    private Integer MAX_SIZE;

    public TestQueue(Integer MAX_SIZE) {
        this.MAX_SIZE = MAX_SIZE;
        blocklist = new ArrayList<String>();

    }

    public synchronized String getMessage() throws InterruptedException {
        if (blocklist.size() < 1) {
            System.out.println("queue is empty now .. going to wait ...");
            wait();
            System.out.println("some one notifyied this thread . i think i can go to work !!");
        }
        Thread.sleep(10000);
        String msg = blocklist.get(0);
        notify();
        return msg;
    }

    public synchronized void putMessage(String msg) throws InterruptedException {
        if (blocklist.size() == MAX_SIZE) {
            System.out.println("Ahh i am full , cant take more right now , will have to wait untill someother thread takes it ");
            wait();
            System.out.println("I think i hv got some space. back to work ");
        }
        System.out.println("Current size = >" +blocklist.size());
        blocklist.add(msg);
        notify();
    }
}
