package com.concurrency.prodconsumer;

public class ProducerConsumerMain {
    public static void main(String [] args) {
        TestQueue testQueue = new TestQueue(10);
    Thread threadOne = new Thread( new Consumer(testQueue),"Consumer Thread");
    Thread threadTwo = new Thread(new Producer(testQueue),"Producer Thread");

    threadOne.start();
    threadTwo.start();
    }
}
