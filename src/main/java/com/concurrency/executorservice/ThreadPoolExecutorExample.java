package com.concurrency.executorservice;

import java.util.concurrent.*;

public class ThreadPoolExecutorExample {
    public static void main(String[] args) {
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        //creating the ThreadPoolExecutor
        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 4, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(),threadFactory);
        for (int i = 0; i < 10; i++) {
            ExecutorTask executorTask = new ExecutorTask("Task " + i);
            System.out.println("A new task has been added : " + executorTask.getName());
            //executor.execute(executorTask);
            executor.submit(executorTask);
        }
         System.out.println("Maximum threads inside pool " + executor.getMaximumPoolSize());
        executor.shutdown();
    }
}

class ExecutorTask implements Runnable {
    private String name;

    public ExecutorTask(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {
        try {
            Long duration = (long) (Math.random() * 10);
            System.out.println("Doing a task during : " + name + " - " + Thread.currentThread().getName());
            TimeUnit.SECONDS.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}