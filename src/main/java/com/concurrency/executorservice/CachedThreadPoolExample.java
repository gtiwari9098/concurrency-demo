package com.concurrency.executorservice;

import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

//newCachedThreadPool method creates an executor having an expandable thread pool.
// A cached thread pool is useful when tasks submitted for processing should not wait and needs
//to be addressed as soon as submitted.
//Such an executor is suitable for applications that launch many short-lived tasks.
//A cached thread pool can have up to 2³¹ no of threads. Effectively, this pool is for short lived asynchronous tasks.

// example given can be used with custom threadfactory as well
public class CachedThreadPoolExample {

    /**
     * Custom thread pool name
     */
    private static final String CUSTOM_CACHED_POOL = "CustomCachedThreadPool";

    public static void main(final String[] arguments) throws InterruptedException {
        ExecutorService executor = Executors.newCachedThreadPool();

        // a custome thread factory be also provided
        ExecutorService executorService = Executors.newCachedThreadPool(new AppThreadFactory(CUSTOM_CACHED_POOL));

        // Cast the object to its class type
        ThreadPoolExecutor pool = (ThreadPoolExecutor) executor;

        //Stats before tasks execution
        System.out.println("Largest executions: "
                + pool.getLargestPoolSize());
        System.out.println("Maximum allowed threads: "
                + pool.getMaximumPoolSize());
        System.out.println("Current threads in pool: "
                + pool.getPoolSize());
        System.out.println("Currently executing threads: "
                + pool.getActiveCount());
        System.out.println("Total number of threads(ever scheduled): "
                + pool.getTaskCount());

        executor.submit(new Task());
        executor.submit(new Task());

        //Stats after tasks execution
        System.out.println("Core threads: " + pool.getCorePoolSize());
        System.out.println("Largest executions: "
                + pool.getLargestPoolSize());
        System.out.println("Maximum allowed threads: "
                + pool.getMaximumPoolSize());
        System.out.println("Current threads in pool: "
                + pool.getPoolSize());
        System.out.println("Currently executing threads: "
                + pool.getActiveCount());
        System.out.println("Total number of threads(ever scheduled): "
                + pool.getTaskCount());

        executor.shutdown();
    }

    static class Task implements Runnable {

        public void run() {

            try {
                Long duration = (long) (Math.random() * 5);
                System.out.println("Running Task! Thread Name: " +
                        Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(duration);
                System.out.println("Task Completed! Thread Name: " +
                        Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class AppThreadFactory implements ThreadFactory {

    private String poolName;
    private ThreadGroup group;

    private AtomicInteger atomicInteger = new AtomicInteger();

    public AppThreadFactory(String poolName) {
        this.poolName = poolName;
        SecurityManager s = System.getSecurityManager();
        group = Objects.nonNull(s) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Thread t = new AppThread(group, runnable, poolName, atomicInteger.incrementAndGet());
        if(t.isDaemon()) {
            t.setDaemon(false);
        }
        if(t.getPriority() != Thread.NORM_PRIORITY) {
            t.setPriority(Thread.NORM_PRIORITY);
        }
        return t;
    }
}

class AppThread extends Thread{

    private static final String POOL_DELIMITER = "-";
    /**
     * Application thread
     *
     * @param runnable, tasks to be processed
     * @param pool, name of the pool
     * @param id, Identifier for the thread
     */
    public AppThread(ThreadGroup group, Runnable runnable, String pool, int id) {
        super(group, runnable, String.format("%s%s%d",pool, POOL_DELIMITER, id));
    }
}
