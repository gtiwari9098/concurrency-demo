package com.concurrency.executorservice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorServiceExample {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        // SingleThreadExecutor
        ExecutorService executorService =
                new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS,
                        new LinkedBlockingQueue<Runnable>());
        Runnable runnableTask = () -> {
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Callable<String> callableTask = () -> {
            TimeUnit.SECONDS.sleep(10);
            return "Task's execution";
        };

        List<Callable<String>> callableTasks = new ArrayList<>();
        callableTasks.add(callableTask);
        callableTasks.add(callableTask);
        callableTasks.add(callableTask);
        executorService.execute(runnableTask);

        Future<String> future =
                executorService.submit(callableTask);

        String result = null;
        try {
            result = future.get();
            System.out.println("Future response >"+result);
            //String result = future.get(200, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
      //  boolean canceled = future.cancel(true);
        boolean isCancelled = future.isCancelled();

       //  String result = executorService.invokeAny(callableTasks);

        // List<Future<String>> futures = executorService.invokeAll(callableTasks);


        executorService.shutdown();
        // List<Runnable> notExecutedTasks = executorService.shutDownNow();
       /* try {
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }*/
        executor.shutdown();
    }
}
