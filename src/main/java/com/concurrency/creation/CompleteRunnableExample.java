package com.concurrency.creation;

public class CompleteRunnableExample implements Runnable {
    private boolean stoppit = false;

    public synchronized void stopThis() {
        stoppit = true;
    }

    @Override
    public void run() {

        while (running()) {
            System.out.println("thread running now!!");
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private boolean running() {
        return stoppit == false;
    }
}
