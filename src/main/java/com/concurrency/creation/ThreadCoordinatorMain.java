package com.concurrency.creation;

public class ThreadCoordinatorMain {
    public static void main (String[] args) {
        ThreadOne threadOne = new ThreadOne();
        threadOne.start();

        ThreadRunnableTwo threadRunnableTwo = new ThreadRunnableTwo();
        Thread thread = new Thread(threadRunnableTwo);
        thread.start();

        // create thread using anonymous inner class

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("runnable thread created using anonymous class");
            }
        };

        // runnable using lambda expression
        Runnable lambdaRunnable = () -> System.out.println("lamda runnable running");
        Thread lambdaRunnableThread = new Thread(lambdaRunnable,"Runnable Thread-1");
        lambdaRunnableThread.start();
    }
}
