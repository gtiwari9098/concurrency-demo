package com.concurrency.creation;

public class SynchronisedStatic {
    public static synchronized void log1(String msg1, String msg2){

    }


    public static void log2(String msg1, String msg2){
        synchronized(SynchronisedStatic.class){

        }
    }
}
