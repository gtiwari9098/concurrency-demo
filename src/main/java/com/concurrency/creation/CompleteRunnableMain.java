package com.concurrency.creation;

public class CompleteRunnableMain {

    public static void main (String[] args) {

        CompleteRunnableExample completeRunnableExample = new CompleteRunnableExample();
        Thread thread = new Thread(completeRunnableExample);
        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){

            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
                System.out.print(thread.getName());
            }
        });
        thread.start();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        completeRunnableExample.stopThis();
    }
}
