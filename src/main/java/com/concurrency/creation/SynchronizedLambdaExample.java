package com.concurrency.creation;

import java.util.function.Consumer;

public class SynchronizedLambdaExample {

    public static void main(String[] args) {
        Consumer<String> func = (String param) -> {
            synchronized (SynchronizedLambdaExample.class) {
                System.out.println(
                        Thread.currentThread().getName() +
                                " step 1: " + param);
                try {
                    Thread.sleep((long) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(
                        Thread.currentThread().getName() +
                                " step 2: " + param);
            }
        };
        Thread thread1 = new Thread(() -> {
            func.accept("Parameter 1");
        }, "Thread 1");

        Thread thread2 = new Thread(() -> {
            func.accept("Parameter 2");
        }, "Thread 2");

        thread1.start();
        thread2.start();
    }

}