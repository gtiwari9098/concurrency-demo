package com.concurrency.cyclicbarrier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierMain {

    private static final Logger logger = LoggerFactory.getLogger(CyclicBarrierMain.class);

    public static void main(String args[]) {
        //creating CyclicBarrier with 3 parties i.e. 3 Threads needs to call await()
        final CyclicBarrier cb = new CyclicBarrier(3, new Runnable() {
            @Override
            public void run() {
                //This task will be executed once all thread reaches barrier
                System.out.println("All parties are arrived at barrier, lets play");
            }
        });

        Thread t1 = new Thread(new Task(cb), "Thread 1");
        Thread t2 = new Thread(new Task(cb), "Thread 2");
        Thread t3 = new Thread(new Task(cb), "Thread 3");
        t1.start();
        t2.start();
        t3.start();

    } //Runnable task for each thread

    private static class Task implements Runnable {
        private CyclicBarrier barrier;

        public Task(CyclicBarrier barrier) {
            this.barrier = barrier;
        }

        @Override
        public void run() {
            try {
                //while(true) {
                    System.out.println(Thread.currentThread().getName() + " is waiting on barrier");
                    Thread.sleep(2000);
                    barrier.await();
                    Thread.sleep(2000);
                    System.out.println(Thread.currentThread().getName() + " has crossed the barrier");
                //}
            } catch (InterruptedException ex) {
                logger.info(null, ex);
            } catch (BrokenBarrierException ex) {
                logger.error(null, ex);
            }
        }
    }
}
