package com.concurrency.concurrenthashmap;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrenHashMapExample {
    public static void main(String[] args)
    {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        concurrentHashMap.put(100, "Hello");
        concurrentHashMap.put(101, "Geeks");
        concurrentHashMap.put(102, "Geeks");

        // Here we cant add Hello because 101 key
        // is already present in ConcurrentHashMap object
        concurrentHashMap.putIfAbsent(101, "Hello");

        // We can remove entry because 101 key
        // is associated with For value
        concurrentHashMap.remove(101, "Geeks");

        // Now we can add Hello
        concurrentHashMap.putIfAbsent(103, "Hello");

        // We cant replace Hello with For
        concurrentHashMap.replace(101, "Hello", "For");
        System.out.println(concurrentHashMap);
        concurrentHashMap.computeIfAbsent("PC", k -> 60000);
        concurrentHashMap.computeIfAbsent("Charger", k -> 800);

        // print new mapping
        System.out.println("new ConcurrentHashMap :\n "
                + concurrentHashMap);

        // Inserting non-existing key along with value
        String returned_value = (String)concurrentHashMap.putIfAbsent(108, "All");

    }
}
